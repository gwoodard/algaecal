

        <?php wp_footer(); ?>
    </section>
    <h3 style="text-align: center;color: #008ba4;"><?php _e( 'Scientific References', 'algae_cal_landing' ); ?></h3>
    <ol style="margin-left: 85px;">
        <li>
            <?php _e( 'Marques A, Ferreira RJ, Santos E, et al. The accuraccy of osteoporotic fracture risk prediction tools: a systematic review and meta-analysis. Ann Reheum Dis. 2015 Nov;74(11):1958-67. doi: 10.1136/anrheumdis-2015-207907. Epub 2015 Aug 6. PMID: 26248637', 'algae_cal_landing' ); ?>
        </li>
        <li>
            <?php _e( 'Riggs BL, Melton LJ 3rd. The worldwid problem of osteoporosis: insights afforded by epidemiology. Bone. 1995 Nove;17(5 Suppl):505S-511S. PMID: 8573428', 'algae_cal_landing' ); ?>
        </li>
        <li>
            <?php _e( 'https://www.algaecal.com/expert-insights/prescription-drugs-that-cause-osteoporosis/', 'algae_cal_landing' ); ?>
        </li>
    </ol>
    <section class="text-center">
        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/GuaranteeSeal.png" style="margin-right: -15px;z-index: 9;position: relative;"/>
        <a href="https://www.algaecal.com/product/bone-builder-packs/ " class="btn btn-info"><?php _e( 'Buy Now', 'algae_cal_landing' ); ?><i class="fa fa-play" style="padding-left: 10px;"></i></a>
    </section>
    <footer class="text-center">
        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Logo.png" width="200"/>
        <p><a href="#"><?php _e( 'Shipping &amp; Returns', 'algae_cal_landing' ); ?></a> <?php _e( '|', 'algae_cal_landing' ); ?> <a href="#"><?php _e( 'Health Disclaimer', 'algae_cal_landing' ); ?></a> <?php _e( '|', 'algae_cal_landing' ); ?> <a href="#"><?php _e( 'Legal and Privacy Policy', 'algae_cal_landing' ); ?></a> <?php _e( '|', 'algae_cal_landing' ); ?> <a href="#"><?php _e( 'Contact', 'algae_cal_landing' ); ?></a> <?php _e( '|', 'algae_cal_landing' ); ?> <a href="#"><?php _e( 'Order Now', 'algae_cal_landing' ); ?></a></p>
        <p> <?php _e( 'Copyright (c) 2017 AlgaeCal', 'algae_cal_landing' ); ?></p>
    </footer>
</body>
