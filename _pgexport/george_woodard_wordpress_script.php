function latest_jquery()
{
  wp_register_script('customscripts', get_template_directory_uri() . 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', array('jquery'), '1.0.0', true);
  wp_enqueue_script('customscripts');
}

add_action('wp_enqueue_scripts', 'custom_scripts_method');
