<!doctype html>
<html style="height:100%;" <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="My new website"/>
        <!-- Core CSS -->
        <!-- Style Library -->
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
        <?php wp_head(); ?>
    </head>
    <body data-spy="scroll" data-target="nav" class="<?php echo implode(' ', get_body_class()); ?>">