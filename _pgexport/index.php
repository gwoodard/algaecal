<?php
get_header(); ?>

<header id="header-2" class="soft-scroll header-2">
    <section class="container">
        <section class="navbar-header">
            <a href="<?php echo esc_url( home_url() ); ?>"> <?php echo wp_get_attachment_image( get_theme_mod( 'blocks_header_2_logo' ), 'large', null, array(
                        'class' => 'brand-img img-responsive'
                ) ) ?> </a>
            <p> <?php _e( 'Tap to Talk 1-800-820-0184', 'algae_cal_landing' ); ?> <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-flag fa-stack-1x fa-inverse"></i></span><?php _e( 'Speak To Our Bone Health Specialist!', 'algae_cal_landing' ); ?> </p>
        </section>
    </section>
</header>
<section class="jumbotron text-left" style="background-image: url('<?php echo esc_url( get_template_directory_uri() ); ?>/images/HeroBG.png');background-repeat: repeat;background-position: center center;color: #ffffff;">
    <h2 class="background" style="text-align: center;font-weight: 500;text-transform: uppercase;"><?php _e( 'Introducing', 'algae_cal_landing' ); ?></h2>
    <p style="color: #ffffff;text-align: center;font-weight: 600;"><?php _e( 'The ONLY Calcium  Supplement that Increases Bone  Density* ...or Your Money Back', 'algae_cal_landing' ); ?></p>
</section>
<p style="text-align: center;"><?php _e( 'Build', 'algae_cal_landing' ); ?> <strong><?php _e( 'brand new bone', 'algae_cal_landing' ); ?></strong> <?php _e( 'WITHIN 6 MONTHS with this', 'algae_cal_landing' ); ?> <strong><?php _e( 'rare algae', 'algae_cal_landing' ); ?></strong> <?php _e( 'calcium ...even if you\'re 85!', 'algae_cal_landing' ); ?></p>
<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
    <div class="wistia_responsive_wrapper" style="height:100%;left:0;position:absolute;top:0;width:100%;">
        <div class="wistia_embed wistia_async_cecdwaq3dz videoFoam=true " style="height:100%;width:100%">&nbsp;</div>
    </div>
</section>
<section style="margin-bottom: 80px; margin-top: 80px;">
    <section class="container">
        <section class="row text-center">
            <!-- Start 3 Month Supply -->
            <section class="col-sm-4 price-block text-center" style="color: #000000;font-family: Roboto;box-shadow: 0 0 5px 5px #dbdbdb;margin-bottom: 70px;margin-right: 40px;margin-left: 40px;">
                <section class="circle">
                    <p style="text-align: center;"><?php _e( '20%', 'algae_cal_landing' ); ?><br><?php _e( 'Off', 'algae_cal_landing' ); ?></p>
                </section>
                <h3 style="text-align: center;padding-top: 20px;"><?php _e( '3 Month Supply', 'algae_cal_landing' ); ?></h3>
                <h3 style="text-align: center;padding-top: 0px;"><?php _e( '4 AlgaeCal Plus', 'algae_cal_landing' ); ?></h3>
                <h3 style="text-align: center;"><?php _e( '3 Strontium Boost', 'algae_cal_landing' ); ?></h3>
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/3MonthSupply.png" width="222" height="135">
                <h2 style="text-align: center;"> <sup><?php _e( '$', 'algae_cal_landing' ); ?></sup><?php _e( '233', 'algae_cal_landing' ); ?><sup> <u><?php _e( 'USD', 'algae_cal_landing' ); ?></u></sup></h2>
                <h4 style="text-align: center;background-image: url('<?php echo esc_url( get_template_directory_uri() ); ?>/images/bgHighlightYellow.png');background-repeat: no-repeat;background-position: center center;"><?php _e( 'Only', 'algae_cal_landing' ); ?> <sup><?php _e( '$', 'algae_cal_landing' ); ?></sup><?php _e( '2.58/Day', 'algae_cal_landing' ); ?></h4>
                <section class="price-footer" style="text-align: center;">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/GuaranteeSeal.png" style="margin-right: -15px;z-index: 9;position: relative;"/>
                    <a href="https://www.algaecal.com/product/bone-builder-packs/ " class="btn btn-info"><?php _e( 'Add to Cart', 'algae_cal_landing' ); ?><i class="fa fa-play" style="padding-left: 10px;"></i></a>
                </section>
            </section>
            <!-- End 3 Month Supply -->
            <!-- Start 6 Month Supply -->
            <section class="col-sm-4 price-block text-center" style="color: #000000;font-family: Roboto;box-shadow: 0 0 5px 5px #dbdbdb;margin-bottom: 70px;margin-right: 40px;margin-left: 40px;">
                <section class="circle">
                    <p style="text-align: center;"><?php _e( '35%', 'algae_cal_landing' ); ?><br><?php _e( 'Off', 'algae_cal_landing' ); ?></p>
                </section>
                <h3 style="text-align: center;padding-top: 20px;"><?php _e( '6 Month Supply', 'algae_cal_landing' ); ?></h3>
                <h3 style="text-align: center;padding-top: 0px;"><?php _e( '8 AlgaeCal Plus', 'algae_cal_landing' ); ?></h3>
                <h3 style="text-align: center;"><?php _e( '6 Strontium Boost', 'algae_cal_landing' ); ?></h3>
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/6MonthSupply.png" width="222" height="135">
                <h2 style="text-align: center;"><sup><?php _e( '$', 'algae_cal_landing' ); ?></sup><?php _e( '337', 'algae_cal_landing' ); ?><sup> <u><?php _e( 'USD', 'algae_cal_landing' ); ?></u></sup></h2>
                <h4 style="text-align: center;background-image: url('<?php echo esc_url( get_template_directory_uri() ); ?>/images/bgHighlightYellow.png');background-repeat: no-repeat;background-position: center center;"><?php _e( 'Only', 'algae_cal_landing' ); ?> <sup><?php _e( '$', 'algae_cal_landing' ); ?></sup><?php _e( '2.09/Day', 'algae_cal_landing' ); ?></h4>
                <section class="price-footer" style="text-align: center;">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/GuaranteeSeal.png" style="margin-right: -15px;z-index: 9;position: relative;"/>
                    <a href="https://www.algaecal.com/product/bone-builder-packs/ " class="btn btn-info"><?php _e( 'Add to Cart', 'algae_cal_landing' ); ?><i class="fa fa-play" style="padding-left: 10px;"></i></a>
                </section>
            </section>
            <!-- End 6 Month Supply -->
            <!-- Start 12 Month Supply -->
            <section class="col-sm-4 price-block text-center" style="color: #000000;font-family: Roboto;box-shadow: 0 0 5px 5px #dbdbdb;margin-bottom: 70px;margin-right: 40px;margin-left: 40px;">
                <section class="circle">
                    <p style="text-align: center;"><?php _e( '38%', 'algae_cal_landing' ); ?><br><?php _e( 'Off', 'algae_cal_landing' ); ?></p>
                </section>
                <h3 style="text-align: center;padding-top: 20px;"><?php _e( '3 Month Supply', 'algae_cal_landing' ); ?></h3>
                <h3 style="text-align: center;padding-top: 0px;"><?php _e( '4 AlgaeCal Plus', 'algae_cal_landing' ); ?></h3>
                <h3 style="text-align: center;"><?php _e( '3 Strontium Boost', 'algae_cal_landing' ); ?></h3>
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/12MonthSupplyBottles.png" width="222" height="135">
                <h2 style="text-align: center;"><sup><?php _e( '$', 'algae_cal_landing' ); ?></sup><?php _e( '717', 'algae_cal_landing' ); ?><sup> <u><?php _e( 'USD', 'algae_cal_landing' ); ?></u></sup></h2>
                <h4 style="text-align: center;background-image: url('<?php echo esc_url( get_template_directory_uri() ); ?>/images/bgHighlightYellow.png');background-repeat: no-repeat;background-position: center center;"><?php _e( 'Only', 'algae_cal_landing' ); ?> <sup><?php _e( '$', 'algae_cal_landing' ); ?></sup><?php _e( '1.97/Day', 'algae_cal_landing' ); ?></h4>
                <section class="price-footer" style="text-align: center;">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/GuaranteeSeal.png" style="margin-right: -15px;z-index: 9;position: relative;"/>
                    <a href="https://www.algaecal.com/product/bone-builder-packs/ " class="btn btn-info"><?php _e( 'Add to Cart', 'algae_cal_landing' ); ?><i class="fa fa-play" style="padding-left: 10px;"></i></a>
                </section>
            </section>
            <!-- End 12 Month Supply -->
        </section>
    </section>
</section>
<!-- Start Supplement Facts -->
<!-- End Supplement Facts -->
<section class="text-center" style="padding-bottom: 15px;">
    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/ACFacts.png"/>
</section>
<section>
    <section class="text-center">
        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/SBFacts.png" style="padding-bottom: 15px;"/>
    </section>
</section>
<section class="text-center">
    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/GuaranteeSeal.png" width="200"/>
    <h3 style="text-align: center;color: #e3742c;font-weight: 600;font-family: Roboto;"><?php _e( 'Stronger Bones for 7 Year Guarantee', 'algae_cal_landing' ); ?></h3>
    <p><?php _e( 'When you follow directions for use of AlgaeCal Plus and Strontium Boost - we guarantee you will see increased bone density in EVERY follow-up bone scan you have while using these 2 products - or we will refund every penny you paid for our products between your scans. This guarantee extends to every scan you have for the next 7 years! Also, if you are unsatisfied at any time you can return my product for a full refund, no questions asked!', 'algae_cal_landing' ); ?> <a href="#"><?php _e( 'Click Here for Details', 'algae_cal_landing' ); ?></a></p>
</section>
<table class="table" style="margin-top: 35px;box-shadow: 12px 0px 11px -1px;">
    <thead>
        <tr>
            <th style="background-color: #4b813d;color: #ffffff;font-family: Roboto;" class="text-center"><?php _e( 'As Seen On:', 'algae_cal_landing' ); ?></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center">
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/DROz.png"/>
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/FoxNews.png" style="margin-left: 15px;"/>
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/PBS.png" style="margin-left: 15px;"/>
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/Amazon.png" style="margin-left: 15px;"/>
                <br>
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/ForeverYoung.png"/>
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/YourBones.png"/>
            </td>
        </tr>
    </tbody>
</table>        

<?php get_footer(); ?>